package org.morpion.unitTest;

import org.junit.Assert;
import org.junit.Test;
import org.morpion.Player;
import org.morpion.Grille;

public class PlayerTest {

    @Test
    public void testMove(){
        Grille grille = new Grille();
        Player player = new Player(grille);
        player.move("4");
        Assert.assertFalse(grille.isSpaceFree(4));
    }


}
