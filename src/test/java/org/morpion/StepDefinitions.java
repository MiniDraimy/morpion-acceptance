package org.morpion;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

public class StepDefinitions {
    @Given("la grille est vide")
    public void laGrilleEstVide() {
        Grille grille = new Grille();
        grille.initGrille();
    }

    @When("tour du joueur")
    public void tourDuJoueur() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        System.out.println();
        player.move("3");
    }

    @Then("l'ordinateur ne joue pas")
    public void lOrdinateurNeJouePas() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
    }

    @When("tour de ordinateur")
    public void tourDeOrdinateur() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        computer.move();
    }

    @Then("ordianteur joue")
    public void ordinateurJoue() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        computer.move();
    }

    @Given("La grille est pleine")
    public void laGrilleEstPleine() {
        Grille grille = new Grille();
        grille.initGrille();
        grille.insertLetter('X',1);
        grille.insertLetter('O',2);
        grille.insertLetter('X',3);
        grille.insertLetter('O',4);
        grille.insertLetter('X',5);
        grille.insertLetter('O',6);
        grille.insertLetter('O',7);
        grille.insertLetter('X',8);
        grille.insertLetter('O',9);
    }

    @Then("L'ordinateur joue sur la case du milieu")
    public void lOrdinateurJoueSurLaCaseDuMilieu() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        computer.firstMove();
    }

    @Then("L'ordinateur joue en case {int}")
    public void lOrdinateurJoueEnCase(int arg0) {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        computer.move();
        grille.insertLetter(computer.mark, arg0);
    }

    @Given("La case du milieu est pleine")
    public void laCaseDuMilieuEstPleine() {
        Grille grille = new Grille();
        grille.initGrille();
        grille.insertLetter('O',5);
    }

    @And("les autres cases sont vides")
    public void lesAutresCasesSontVides() {
        Grille grille = new Grille();
        grille.initGrille();
    }

    @Then("L'ordinateur joue dans un coin")
    public void lOrdinateurJoueDansUnCoin() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        grille.insertLetter(computer.mark,1 );
    }

    @Given("deux cases sur la même ligne ou cologne ou diagonales sont occupées par le joueur")
    public void deuxCasesSurLaMêmeLigneOuCologneOuDiagonalesSontOccupéesParLeJoueur() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        grille.insertLetter(player.mark, 1);
        grille.insertLetter(player.mark, 4);
    }

    @Then("l'ordinateur joue sur la troisieme case")
    public void lOrdinateurJoueSurLaTroisiemeCase() {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        grille.insertLetter(computer.mark, 7);
    }

    @Given("la case {int} est a l'ordinateur")
    public void laCaseEstALOrdinateur(int arg0) {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        grille.insertLetter(computer.mark, arg0);
    }

    @And("case {int} libre")
    public void caseLibre(int arg0) {
        Grille grille = new Grille();
        grille.initGrille();
        grille.isSpaceFree(arg0);
    }

    @Then("l'ordinateur joue sur la case {int}")
    public void lOrdinateurJoueSurLaCase(int arg0) {
        Grille grille = new Grille();
        Player player = new Player(grille);
        Computer computer = new Computer(grille,player);
        computer.move();
        grille.insertLetter(computer.mark, arg0);
    }
}
